<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h1>Spiele</h1>
  <p>Übungsseite zu XSL-Templates.</p>
  
  <h2>Alle Datensätze</h2>
  <p>Zeigt alle Datensätze an, ohne eine spezielle Formatierung oder Auswahl.</p>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>ArtNr</th>
      <th>Name</th>
      <th>FSK</th>
      <th>Preis</th>
      <th>Anzahl</th>
    </tr>
    <xsl:for-each select="games/game">
	    <tr>
	      <td><xsl:value-of select="id"/></td>
	      <td><xsl:value-of select="name"/></td>
	      <td><xsl:value-of select="fsk"/></td>
	      <td><xsl:value-of select="price"/>€</td>
	      <td><xsl:value-of select="amount"/></td>
	    </tr>
    </xsl:for-each>
  </table>
  
  <h2>Sortiert nach Name</h2>
  <p></p>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>ArtNr</th>
      <th>Name</th>
      <th>FSK</th>
      <th>Preis</th>
      <th>Anzahl</th>
    </tr>
    <xsl:for-each select="games/game">
    	<xsl:sort select="name"/>
	    <tr>
	      <td><xsl:value-of select="id"/></td>
	      <td><xsl:value-of select="name"/></td>
	      <td><xsl:value-of select="fsk"/></td>
	      <td><xsl:value-of select="price"/>€</td>
	      <td><xsl:value-of select="amount"/></td>
	    </tr>
    </xsl:for-each>
  </table>
  
  <h2>Preis 10€</h2>
  <p>Zeigt alle Spiele an, die 10€ kosten (oder mehr).</p>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th>ArtNr</th>
      <th>Name</th>
      <th>FSK</th>
      <th>Preis</th>
      <th>Anzahl</th>
    </tr>
    <xsl:for-each select="games/game">
    	<xsl:if test="number(price) >= 10.0">
		    <tr>
		      <td><xsl:value-of select="id"/></td>
		      <td><xsl:value-of select="name"/></td>
		      <td><xsl:value-of select="fsk"/></td>
		      <td><xsl:value-of select="price"/>€</td>
		      <td><xsl:value-of select="amount"/></td>
		    </tr>
		</xsl:if>
    </xsl:for-each>
  </table>
  
  <h2>Datensätze als Liste</h2>
  <p>Zeigt die Namen der Datensätze geordnet als Liste an.</p>
  	<ul>
  	<xsl:for-each select="games/game">
  		<xsl:sort select="name"/>
		<li><xsl:value-of select="name"/></li>
	</xsl:for-each>
	</ul>
	
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>