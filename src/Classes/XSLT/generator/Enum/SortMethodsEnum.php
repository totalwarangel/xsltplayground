<?php
namespace Xsltp\XsltPlayground\Classes\XSLT\generator\Enum;

class SortMethodsEnum
{
    public static $TYPES = array(
        '1' => null,
        '2' => 'name',
        '3' => 'amount',
        '4' => 'id',
    );
}

