<?php
namespace Xsltp\XsltPlayground\Classes\XSLT\generator\Enum;

class FilterRelationEnum
{
    public static $TYPES = array(
        '1' => '&lt;=',
        '2' => '&gt;=',
        '3' => '=',
    );
}

