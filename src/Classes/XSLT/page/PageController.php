<?php
namespace Xsltp\XsltPlayground\Classes\XSLT\page;

class PageController
{
    /**
     * Twig object
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Path to template
     * @var string
     */
    private $tpl;

    /**
     * Parameters for the template
     * @var array
     */
    private $data;

    /**
     * Constructor
     *
     * @param \Twig_Environment $twig Twig object
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Renders the template with the arguments passed by data array.late
     *
     * @return string Rendered template
     */
    public function render()
    {
        return $this->twig->render($this->tpl, $this->data);
    }

    /**
     * Sets the location of the template.
     *
     * @param string $tpl
     */
    public function setTpl($tpl)
    {
        $this->tpl = $tpl;
    }

    /**
     * Sets the data array for the template.
     *
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}