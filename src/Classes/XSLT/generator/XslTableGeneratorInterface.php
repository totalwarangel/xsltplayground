<?php

namespace Xsltp\XsltPlayground\Classes\XSLT\generator;

interface XslTableGeneratorInterface
{
    /**
     * Defines the name of the template. The template will be saved under
     * this name.
     *
     * @param string $name
     */
    public function setName(string $name) : void;

    /**
     * Add an tag to be shown in the generated table.
     *
     * @param string $tag Name of tag
     * @param string $name Display name
     */
    public function addColumn(string $tag, string $name) : void;

    /**
     * Set a condition for the displayed table rows.
     *
     * @param string $condition Condition string
     */
    public function setCondition(string $condition) : void;

    /**
     * Sets the sort condition.
     *
     * @param string $condition Sort condition
     */
    public function sortBy(string $condition) : void;

    /**
     * Creates the Template
     */
    public function save() : void;
}

