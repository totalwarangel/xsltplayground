<?php

use Xsltp\XsltPlayground\Classes\XSLT\generator\Enum\FilterMethodsEnum;
use Xsltp\XsltPlayground\Classes\XSLT\generator\Enum\FilterRelationEnum;
use Xsltp\XsltPlayground\Classes\XSLT\generator\Enum\SortMethodsEnum;
use Xsltp\XsltPlayground\Classes\XSLT\generator\Impl\XslTableGeneratorTwigImpl;

require_once '../../vendor/autoload.php';

$sortMethod     = (isset($_POST['sortmeth'])) ? $_POST['sortmeth'] : '1';
$filterMethod   = (isset($_POST['filtermeth'])) ? $_POST['filtermeth'] : '1';
$filterRelation = (isset($_POST['filterrel'])) ? $_POST['filterrel'] : '1';

$filterValue;
if (isset($_POST['filterval'])) {
    if (strstr($_POST['filterval'], '.') == false) {
        $filterValue = intval($_POST['filterval']);
    } else {
        $filterValue = floatval($_POST['filterval']);
    }
}
if ($filterValue < 0) {
    $filterValue = 0;
}

$smString = SortMethodsEnum::$TYPES[$sortMethod];
$fmString = FilterMethodsEnum::$TYPES[$filterMethod];
$frString = FilterRelationEnum::$TYPES[$filterRelation];

/**
 * @var XslTableGeneratorInterface
 */
$generator = new XslTableGeneratorTwigImpl(
    '../../files/xmldb/criteria.xsl'
);
$generator->addColumn('id', 'Artikelnummer');
$generator->addColumn('name', 'Bezeichnung');
$generator->addColumn('price', 'Preis');
$generator->addColumn('fsk', 'FSK');
$generator->addColumn('amount', 'Menge');

if ($smString != null) {
    $generator->sortBy($smString);
}

if ($fmString != null) {
    $generator->setCondition($fmString . ' ' . $frString . ' ' . $filterValue);
}

$generator->save();

echo 'TEMPLATE_READY';