<?php

use Xsltp\XsltPlayground\Classes\XSLT\page\PageController;

require_once '../../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('../../templates/xml2html');
$twig = new Twig_Environment($loader, array(
    'cache' => false
));

$page = new PageController($twig);
$page->setTpl('main.twig');
$page->setData(array());
echo $page->render();