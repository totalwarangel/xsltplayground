<?php

namespace Xsltp\XsltPlayground\Test;

use PHPUnit\Framework\TestCase;
use Xsltp\XsltPlayground\Classes\XSLT\generator\Impl\XslTableGeneratorImpl;

/**
 * XslTableGeneratorImpl test case.
 */
class XslTableGeneratorImplTest extends TestCase
{

    /**
     *
     * @var XslTableGeneratorImpl
     */
    private $xslTableGeneratorImpl;

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();

        // TODO Auto-generated XslTableGeneratorImplTest::setUp()

        $this->xslTableGeneratorImpl = new XslTableGeneratorImpl('bockwurst');
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        // TODO Auto-generated XslTableGeneratorImplTest::tearDown()
        $this->xslTableGeneratorImpl = null;

        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct()
    {
        // TODO Auto-generated constructor
    }

    /**
     * Tests XslTableGeneratorImpl->save()
     */
    public function testSave()
    {
        $this->xslTableGeneratorImpl->sortBy('name');
        $this->xslTableGeneratorImpl->setCondition('price >= 12.0');
        $this->xslTableGeneratorImpl->addColumn('id', 'Artikelnummer');
        $this->xslTableGeneratorImpl->addColumn('name', 'Bezeichnung');
        $this->xslTableGeneratorImpl->addColumn('price', 'Preis');

        $this->xslTableGeneratorImpl->save();
    }
}

