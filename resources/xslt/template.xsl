<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h1>Spiele</h1>
  <p>Übungsseite zu XSL-Templates.</p>

  <table style="border: 1px solid black">
    <tr bgcolor="#9acd32">
      %s
    </tr>
    <xsl:for-each select="games/game">
	    %s
		    %s
		    <tr>
		    	%s
		    </tr>
	    %s
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>