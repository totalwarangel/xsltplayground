<?php

namespace Xsltp\XsltPlayground\Classes\XSLT\generator\Impl;

use Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface;
use Xsltp\XsltPlayground\Classes\XSLT\page\PageController;


/**
 *
 * @author martin
 *
 */
class XslTableGeneratorTwigImpl implements XslTableGeneratorInterface
{
    /**
     * Display condition
     * @var string
     */
    private $displayCondition = '';

    /**
     * Sort condition
     * @var string
     */
    private $sortCondition = '';

    /**
     * Array of TableColumn objects containing the columns to display.
     * @var array
     */
    private $columns;

    /**
     * Name of the template
     * @var string
     */
    private $name;

    /**
     * The template ready to be written as file.
     * @var string
     */
    private $template;

    /**
     *
     * @var string
     */
    private $source;

    /**
     * Constructor
     *
     * @param string $name Name of template
     * @param string $source Position of base template
     */
    public function __construct(string $name, string $source = 'xsl.twig')
    {
        $this->columns = array();
        $this->name = $name;
        $this->source = $source;
        $this->template = "";
    }

    /**
     *
     * {@inheritDoc}
     * @see \Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface::setName()
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface::addColumn()
     */
    public function addColumn(string $tag, string $name): void
    {
        $column = new TableColumn();
        $column->setTag($tag);
        $column->setName($name);
        array_push($this->columns, $column);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface::setCondition()
     */
    public function setCondition(string $condition): void
    {
        $this->displayCondition = $condition;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface::sortBy()
     */
    public function sortBy(string $condition): void
    {
        $this->sortCondition = $condition;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface::save()
     */
    public function save(): void
    {
        $this->generateTemplate();
        if (strstr($this->name, '.xsl') === false) {
            $this->name .= '.xsl';
        }

        file_put_contents(
            $this->name,
            $this->template
        );
    }

    /**
     *
     */
    private function generateTemplate() : void
    {
        $data = array(
            'columnList'      => $this->columns,
            'filterCondition' => $this->displayCondition,
            'sortCondition'   => $this->sortCondition,
        );

        $loader = new \Twig_Loader_Filesystem('../../templates/xmldb');
        $twig = new \Twig_Environment($loader, array(
            'cache' => false
        ));
        $page = new PageController($twig);
        $page->setTpl($this->source);
        $page->setData($data);
        $this->template = $page->render();
    }
}

