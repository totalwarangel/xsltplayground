<?php
namespace Xsltp\XsltPlayground\Classes\XSLT\generator\Enum;

class FilterMethodsEnum
{
    public static $TYPES = array(
        '1' => null,
        '2' => 'fsk',
        '3' => 'price',
        '4' => 'amount',
    );
}

