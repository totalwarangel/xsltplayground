<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h1>Spiele</h1>
  <p>Übungsseite zu XSL-Templates.</p>

  <table style="border: 1px solid black">
    <tr bgcolor="#9acd32">
      <th>Artikelnummer</th><th>Bezeichnung</th><th>Preis</th><th>FSK</th><th>Menge</th>
    </tr>
    <xsl:for-each select="games/game">
	    <xsl:sort select="name"/>
		    <xsl:if test="fsk &gt;= 16">
		    <tr>
		    	<td><xsl:value-of select="id"/></td><td><xsl:value-of select="name"/></td><td><xsl:value-of select="price"/></td><td><xsl:value-of select="fsk"/></td><td><xsl:value-of select="amount"/></td>
		    </tr>
	    </xsl:if>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>