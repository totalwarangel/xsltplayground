<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/reset.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
	</head>

	<body>
		<div id="page-root">
    		<h1>Testseiten XSLT</h1>
    		<table>
    			<tr>
    				<th>Name</th>
    				<th>Beschreibung</th>
    			</tr>
    			<?php
    			     $files = scandir('contents');
    			     foreach ($files as $v) {
    			         if (is_dir('contents/' . $v)) {
    			             if (!file_exists('contents/' . $v . '/description.csv')) {
    			                 continue;
    			             }
    			             $file = fopen('contents/' . $v . '/description.csv', 'r+');
    			             fseek($file, 0);
    			             $data = fgetcsv($file, 2000, '|');

    			             echo '<tr>';
    			             echo '<td><a href="' . 'contents/' . $v . '">' . $data[0] . '</a></td>';
    			             echo '<td>' . $data[1] . '</td>';
    			             echo '</tr>';
    			         }
    			     }
    			?>
    		</table>
    	</div>
	</body>
</html>