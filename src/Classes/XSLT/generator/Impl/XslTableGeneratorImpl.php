<?php

namespace Xsltp\XsltPlayground\Classes\XSLT\generator\Impl;

use Xsltp\XsltPlayground\Classes\XSLT\generator\XslTableGeneratorInterface;


/**
 *
 * @author martin
 *
 */
class XslTableGeneratorImpl implements XslTableGeneratorInterface
{
    /**
     * Display condition
     * @var string
     */
    private $displayCondition;

    /**
     * Sort condition
     * @var string
     */
    private $sortCondition;

    /**
     * Array of TableColumn objects containing the columns to display.
     * @var array
     */
    private $columns;

    /**
     * Name of the template
     * @var string
     */
    private $name;

    /**
     * The template ready to be written as file.
     * @var string
     */
    private $template;

    /**
     *
     * @var string
     */
    private $source;

    /**
     * Constructor
     *
     * @param string $name Name of template
     * @param string $source Position of base template
     */
    public function __construct(string $name, string $source = 'resources/xslt/template.xsl')
    {
        $this->columns = array();
        $this->name = $name;
        $this->source = $source;
        $this->template = "";
    }

    /**
     * Defines the name of the template. The template will be saved under
     * this name.
     *
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * Add an tag to be shown in the generated table.
     *
     * @param string $tag Name of tag
     * @param string $name Display name
     */
    public function addColumn(string $tag, string $name) : void
    {
        $column = new TableColumn();
        $column->setTag($tag);
        $column->setName($name);
        array_push($this->columns, $column);
    }

    /**
     * Set a condition for the displayed table rows.
     *
     * @param string $condition Condition string
     */
    public function setCondition(string $condition = null) : void
    {
        $this->displayCondition = $condition;
    }

    /**
     * Sets the sort condition.
     *
     * @param string $condition Sort condition
     */
    public function sortBy(string $condition = null) : void
    {
        $this->sortCondition = $condition;
    }

    /**
     *
     */
    private function generateTemplate() : void
    {
        $this->template = file_get_contents($this->source);

        $columnTitle   = '';
        $columnContent = '';
        $columnCond    = '';
        $columnCondEnd = '';
        $columnSort    = '';

        // Sorting
        if ($this->sortCondition != null) {
            $columnSort = '<xsl:sort select="' . $this->sortCondition . '"/>';
        }

        // Condition
        if ($this->displayCondition != null) {
            $columnCond    = '<xsl:if test="' . $this->displayCondition . '">';
            $columnCondEnd = '</xsl:if>';
        }

        // Inhalt
        $amount = sizeof($this->columns);
        if ($amount == 0) {
            $columnTitle   = '<th></th>';
            $columnContent = '<td></td>';
        } else {
            for ($i=0; $i<$amount; $i++) {
                /**
                 * @var TableColumn
                 */
                $data = $this->columns[$i];

                $columnTitle   .= '<th>' . $data->getName() . '</th>';
                $columnContent .= '<td><xsl:value-of select="' . $data->getTag() . '"/></td>';
            }
        }

        $this->template = sprintf($this->template, $columnTitle, $columnSort,
            $columnCond, $columnContent, $columnCondEnd);
    }

    /**
     * Creates the Template
     */
    public function save() : void
    {
        $this->generateTemplate();
        if (strstr($this->name, '.xsl') === false) {
            $this->name .= '.xsl';
        }

        file_put_contents(
            $this->name,
            $this->template
        );
    }
}

