/**
 * Document ready action
 * @returns void
 */
$(document).ready(function() {
	$('#submit').on('click', function(event) {
		submit();
    });
	
	$('#reset').on('click', function(event) {
		resetFields();
    });
	
	$('#filtermeth').on('change', function(event) {
		showAdvancedOptions(this.value);
    });
});

/**
 * Returns the selected value of the sort selection.
 * @returns number
 */
function getSortValue() {
	return Number.parseInt($('#sortmeth').val());
}

/**
 * Returns the selected value of the filter type selection.
 * @returns number
 */
function getFilterMethod() {
	return Number.parseInt($('#filtermeth').val());
}

/**
 * Returns the selected value of the filter relation selection.
 * @returns number
 */
function getFilterRelation() {
	return Number.parseInt($('#filterrel').val());
}

/**
 * Returns the entered Value of the filter.
 * @returns string
 */
function getFilterValue() {
	return $('#filterval').val();
}

/**
 * Resets the fields after submit.
 * @returns void
 */
function resetFields() {
	$('#filterreldiv').addClass("inv");
	$('#filtervaldiv').addClass("inv");
	
	$('#sortmeth').val("1");
	$('#filtermeth').val("1");
	$('#filterrel').val("1");
	$('#filterval').val("0");
}

/**
 * Zeigt oder versteckt die erweiterten Filteroptionen.
 * @param value Ausgewählte Option
 * @returns void
 */
function showAdvancedOptions(value) {
	if (value != "1") {
		$('#filterreldiv').removeClass("inv");
		$('#filtervaldiv').removeClass("inv");
	} else {
		$('#filterreldiv').addClass("inv");
		$('#filtervaldiv').addClass("inv");
	}
}

/**
 * Submits the user input to the server.
 * @returns void
 */
function submit() {
	$.ajax({
        url: "view.php",
        type: 'POST',
        data: {
        	sortmeth: getSortValue(),
        	filtermeth: getFilterMethod(),
        	filterrel: getFilterRelation(),
        	filterval: getFilterValue(),
        },
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        success: function(response) {
        	reload(response);
        },
        error: function() {
            alert("Ein Fehler ist aufgetreten!");
        }
    });
}

/**
 * 
 * @param response Response from server
 * @returns void
 */
function reload(response) {
	if (response != "TEMPLATE_READY") {
		alert("Ein Fehler ist aufgetreten!");
		resetFields();
	}
	$('#xml_frame')[0].contentWindow.location.reload(true);
}