<?php

namespace Xsltp\XsltPlayground\Classes\XSLT\generator\Impl;

/**
 * Model for table colums used by XslTableGeneratorImpl.
 * @author martin
 */
class TableColumn
{
    /**
     * The tag name in the document containing the data
     * @var string
     */
    private $tag;

    /**
     *
     * @var string
     */
    private $name;

    /**
     * Constructor
     *
     * @param string $tag Name of the tag
     * @param string $name Display name
     */
    public function __construct(string $tag = null, string $name = null)
    {
        $this->tag = $tag;
        $this->name = $name;
    }

    /**
     * Returns name of the tag.
     *
     * @return string Tag name
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Returns the display name.
     *
     * @return string Display name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the tag name.
     *
     * @param string $tag Name of the tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * Sets the display name.
     *
     * @param string $name Display Name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}

