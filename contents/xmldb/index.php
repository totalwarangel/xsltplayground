<?php

use Xsltp\XsltPlayground\Classes\XSLT\page\PageController;

require_once '../../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('../../templates/xmldb');
$twig = new Twig_Environment($loader, array(
    'cache' => false
));

$page = new PageController($twig);
$page->setTpl('main.twig');
$page->setData(array(
    'gamesSrc' => '../../resources/xslt/games.xml',

    'sortCriteria' => array(
        'Nichts', 'Name', 'Anzahl', 'Artikelnummer',
    ),

    'filterCriteria'=> array(
        '0' => 'Nichts',
        '1' => 'Jugendfreigabe',
        '2' => 'Preis',
    ),

    'filterRelation'=> array(
        '0' => 'kleiner oder gleich',
        '1' => 'größer oder gleich',
        '2' => 'genau',
    ),
));

echo $page->render();